from django.db import models

# Create your models here.

class Post(models.Model):
    id = models.AutoField(primary_key=True)
    pTitle = models.CharField(max_length=255, null=False)
    pType = models.CharField(max_length=100, null=False)
    pTag = models.CharField(max_length=100, null=True, default=None, blank=True,)
    pDescription = models.CharField(max_length=1000, null=False)
    pDateTime = models.DateTimeField(auto_now=True, auto_now_add=False, )

