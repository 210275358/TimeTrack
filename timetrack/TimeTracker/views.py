from unittest import mock
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django .views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Post

# Create your views here.

class PostListView(ListView):

    model = Post
    paginate_by = 50
    queryset = Post.objects.all()
    template_name = 'forum.html'

class PostCreateView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    model = Post
    fields = '__all__'
    template_name = 'create_post.html'

    def form_valid(self, form):
        model = form.save(commit=False)
        model.save()
        return HttpResponseRedirect(reverse('forum'))

class PostUpdateView(UpdateView):
    model = Post
    fields = ['pTitle', 'pDescription', 'pType']
    template_name = 'update_post.html'

    def get_object(self, queryset=None):
        id = self.kwargs['id']
        return self.model.objects.get(id=id)

    def form_valid(self, form):
        model = form.save(commit=False)
        model.save()
        return HttpResponseRedirect(reverse('forum'))

class PostDeleteView(DeleteView):
    model = Post
    template_name = 'delete_post.html'

    def get_success_url(self):
        return reverse('forum')